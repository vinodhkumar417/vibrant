import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { UrlSettings } from '../AppConfig/url-settings';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  baseUrl = environment.baseUrl;
  private isloggedIn: boolean;
  constructor(private http: HttpClient) {
    this.isloggedIn = false;
  }

  login(username: string, password: string) {
    if (username == 'Admin' && password == 'Admin') {
      let value = [{ 'username': username, 'password': password }]
      localStorage.setItem('login', JSON.stringify(value));
      this.isloggedIn = true;
    }
    else {
      this.isloggedIn = false;
    }
    return of(this.isloggedIn);
  }
  isUserLoggedIn(): boolean {
    return this.isloggedIn;
  }
  logoutUser(): void {
    localStorage.clear();
    this.isloggedIn = false;
  }

  public getAll(url: string): Observable<any> {
    return this.http.get(this.baseUrl + url);
  }

  public getbyId(url:string,Id):Observable<any>{
    return this.http.get(this.baseUrl+url+Id);
  }

  public deleteById(url, Id): Observable<any> {
    return this.http.delete(this.baseUrl + url + Id)
  }
  public Update(url, Id,body): Observable<any> {
    return this.http.put(this.baseUrl + url + Id,body)
  }
  public Create(url,body): Observable<any> {
    return this.http.put(this.baseUrl + url,body)
  }
}
