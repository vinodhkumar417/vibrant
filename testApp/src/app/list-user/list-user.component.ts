import { Component, OnInit } from '@angular/core';
import { UrlSettings } from '../AppConfig/url-settings';
import { UserService } from '../Services/user.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { formPattern } from '../AppConfig/FormPattern';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css'],
})
export class ListUserComponent implements OnInit {
  listing = UrlSettings.Listing;
  DeleteId = UrlSettings.deleteById;
  GetByIdURL = UrlSettings.GetById;
  UpdateByURL = UrlSettings.UpdateById;
  CreatebyUrl = UrlSettings.CreateUser;
  mobilePattern = formPattern.phone;
  pincodePattern = formPattern.pincode;

  DataUserList: any;
  Enabled: boolean;
  submitted: boolean;
  paramId: string;
  UserUpdateForm: FormGroup;
  CreateUserForm: FormGroup;
  CreateEnabled: boolean;

  constructor(private formBuilder: FormBuilder, private service: UserService, private router: Router) { }

  ngOnInit() {
    this.getAlldata();
  }
  // get list
  getAlldata() {
    this.service.getAll(this.listing).subscribe(res => {
      console.log("data list", res);
      this.DataUserList = res.data;
    })
  }

  //delete item
  delete(dt, index) {
    this.service.deleteById(this.DeleteId, dt.id).subscribe(res => {
      this.DataUserList.splice(index, 1);
      console.log("del", res);
    })
  }

  get f() { return this.UserUpdateForm.controls; }
  get c() { return this.CreateUserForm.controls; }

  UpdateForm() {
    this.UserUpdateForm = this.formBuilder.group({
      first_name: ['', [Validators.required, Validators.minLength(4)]],
      last_name: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', [Validators.required, Validators.email]]
    });
  }

  edit(dt, i) {
    this.paramId = dt.id;
    if (this.paramId != undefined) {
      this.service.getbyId(this.GetByIdURL, this.paramId).subscribe(res => {
        this.Enabled = true;
        this.CreateEnabled = false;
        this.UpdateForm();
        this.UserUpdateForm.patchValue(res.data);
      })
    }
    else {
      alert("User ID not found");
    }

  }

  CreateForm() {
    this.CreateUserForm = this.formBuilder.group({
      first_name: ['', [Validators.required, Validators.minLength(4)]],
      last_name: ['', [Validators.required, Validators.minLength(4)]],
      email: ['', [Validators.required, Validators.email]],
      username: ['', [Validators.required, Validators.minLength(4)]],
      phone: ['', [Validators.required, Validators.pattern(this.mobilePattern)]],
      address: ['', [Validators.required, Validators.minLength(10)]],
      pincode: ['', [Validators.required, Validators.pattern(this.pincodePattern)]]
    });
  }

  create() {
    this.CreateEnabled = true;
    this.Enabled = false;
    this.CreateForm();
  }

  onCreateUser() {
    if (this.CreateUserForm.invalid) {
      return;
    }
    else {
      this.service.Create(this.CreatebyUrl, this.CreateUserForm.value).subscribe(res => {
        console.log("create", res);
        this.DataUserList.push(res);
        this.CreateEnabled = false;
      })

    }
  }

  onSubmit() {
    this.submitted = true;

    if (this.UserUpdateForm.invalid) {
      return;
    }
    else {
      this.service.Update(this.UpdateByURL, this.paramId, this.UserUpdateForm.value).subscribe(res => {
        console.log("update", res);
        for (let i = 0; i < this.DataUserList.length; i++) {
          if (this.DataUserList[i].id == this.paramId) {
            this.DataUserList[i].first_name = res.first_name
            this.DataUserList[i].last_name = res.last_name
            this.DataUserList[i].email = res.email
          }
        }
        this.Enabled = false;
      })

    }

  }

}
