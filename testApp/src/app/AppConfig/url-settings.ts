export enum UrlSettings {
    Listing = 'api/users?page=1',
    deleteById = 'api/users/',
    GetById = 'api/users/',
    UpdateById = 'api/users/',
    CreateUser = 'api/users/',

}
