import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListUserComponent } from './list-user/list-user.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthgaurdGuard } from './Services/authgaurd.guard';


const routes: Routes = [
  { path: 'Login', component: LoginComponent },
  { path: 'List', component: ListUserComponent,canActivate :[AuthgaurdGuard] }, 
  { path: '', redirectTo: '/List', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
