import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UserService } from '../Services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  UserLoginForm: FormGroup;
  submitted: boolean;
  constructor(private formBuilder: FormBuilder, private service: UserService, private _router: Router) {
    this.loginForm();
  }

  ngOnInit() {
  }

  loginForm() {
    this.UserLoginForm = this.formBuilder.group({
      Name: ['', [Validators.required, Validators.minLength(4)]],
      password: ['', [Validators.required, Validators.minLength(5)]]
    });
  }

  get f() { return this.UserLoginForm.controls; }

  onSubmit() {
    this.submitted = true;
    if (this.UserLoginForm.invalid) {
      return;
    }
    else {
       console.log("val 2",this.UserLoginForm.value);
      this.service.login(this.UserLoginForm.value.Name, this.UserLoginForm.value.password).subscribe(data => {
        if (data) {
          this.UserLoginForm.reset();
          this.submitted = false;
          this._router.navigate(['/List']);
        }
        else{
          alert("invalid credentials");
        }
      })

    }

  }


}
