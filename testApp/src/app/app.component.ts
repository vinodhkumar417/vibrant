import { Component } from '@angular/core';
import { UserService } from './Services/user.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'testApp';
  constructor(private service:UserService){}

  logout(){
    this.service.logoutUser();
  }
  
}
